//
//  MUltCollectionViewController.swift
//  CollecViewMultipleSecSuppViews
//
//  Created by Sourav on 02/10/18.
//  Copyright © 2018 Sourav. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ONE"
private let reuseIdentifierTWO = "TWO"
private let reuseIdentifierTHREE = "THREE"



class MUltCollectionViewController: UICollectionViewController {
var flowlayout = UICollectionViewFlowLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        flowlayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flowlayout.itemSize = CGSize(width: 50, height: 50)
     // self.collectionView.supplementaryView(forElementKind: , at: 0)
       // self.collectionView.setCollectionViewLayout(flowlayout, animated: true)
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if section == 0{
            return 5
        }
        return 5
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:reuseIdentifier, for: indexPath)
            
            // Configure the cell
            
            return cell
        }else if indexPath.section == 1{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:reuseIdentifierTWO, for: indexPath)
            
            // Configure the cell
            
            return cell
        } else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:reuseIdentifierTHREE, for: indexPath)
            
            // Configure the cell
            
            return cell
        }
      
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
  
        if indexPath.section == 0
        {
            
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as? headerCollectionView
            header!.headerlabel.text = "firstheader"
            return header!
        }
        else if indexPath.section == 1 {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as? headerCollectionView
            header!.headerlabel.text = "firstheader"
            return header!
        }else if indexPath.section == 2 {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as? headerCollectionView
             header!.headerlabel.text = "secondheader"
            return header!
        }else{
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as? headerCollectionView
             header!.headerlabel.text = "thirdheader"
            return header!
        }
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
